import socket


def nginx_dehydrated_deploy(sites, default_domain):
    deploy_list = []
    for site in sites:
        config = {}
        config["id"] = "nginx-{}".format(site["name"])
        if site["domains"]:
            config["domain"] = site["domains"][0]
        else:
            config["domain"] = default_domain
        config["fullchain"] = [
            {"path": "/etc/ssl/nginx/{}.pem".format(site["name"]), "owner": "root"}
        ]
        config["key"] = [
            {"path": "/etc/ssl/nginx/{}.key".format(site["name"]), "owner": "root"}
        ]
        if site["domains"] and not (
            len(site["domains"]) == 1 and config["domain"] == default_domain
        ):
            config["alias"] = "nginx-{}".format(site["name"])
        config["reload_cmd"] = "rc-service nginx restart"
        deploy_list.append(config)
    return deploy_list


def nginx_dehydrated_certs(sites, default_domain):
    cert_list = []
    for site in sites:
        config = {}
        if site["domains"]:
            config["domains"] = site["domains"]
        else:
            config["domains"] = [
                default_domain,
            ]
        if not (len(config["domains"]) == 1 and config["domains"][0] == default_domain):
            config["alias"] = "nginx-{}".format(site["name"])
        cert_list.append(config)
    return cert_list


class FilterModule:
    """nginx role filters"""

    def filters(self):
        return {
            "nginx_dehydrated_deploy": nginx_dehydrated_deploy,
            "nginx_dehydrated_certs": nginx_dehydrated_certs,
        }
